package com.experience.rest

import com.experience.UserActivityRepository
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController()
class UserActivityEndPoint(val userActivityRepository: UserActivityRepository) {

    @PostMapping("/trackActivity")
    fun trackActivity(@RequestBody userActivity: UserActivity) {
        userActivityRepository.saveUserId(userActivity)
    }
}

data class UserActivity(val userId: Long, val searched: String, val clicked_link: String)