package com.experience

import com.experience.rest.UserActivity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

class UserActivityRepository(private val jdbcTemplate: NamedParameterJdbcTemplate) {

    private val insertUserId = "INSERT INTO tourscanner.user_action VALUES(:USER_ID)"

    fun saveUserId(userActivity: UserActivity){
        jdbcTemplate.update(insertUserId, activityParams(userActivity.userId))
    }

    private fun activityParams(userId: Long): MapSqlParameterSource {
        val params = MapSqlParameterSource()
        params.addValue("USER_ID", userId)
        return params
    }
}
